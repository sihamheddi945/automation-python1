from bs4 import BeautifulSoup
import requests as rq
import time

print("enter the name of the job ")
query = input(">> ")
url = "https://www.indeed.com/jobs?q=" + query


def find_job(url):
    response = rq.get(url)
    content = response.content

    soup = BeautifulSoup(content, "html.parser")

    jobs = soup.find("div", id="mosaic-zone-jobcards")

    links = jobs.div.findAll("a", class_="tapItem")

    for l in links:
        date_published = l.find("span", class_="date").text
        if "30+ days" not in date_published:
            print(f" title job => {l.h2.text}")
            print(f"\n company => {l.pre.span.text}")
            print(f"description => {l.li.text}")
            print(f"date of publishing => {date_published.split('Posted')[-1]}")
            print("\n --------------------------------------------------- \n")
while True:
    find_job(url)
    print("Do you want to continue ? (y/n)")
    ans = input(">> ")
    if ans == "y":
        time.sleep(10000)
        continue
    else:
        break