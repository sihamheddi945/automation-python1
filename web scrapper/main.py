from bs4 import BeautifulSoup


f = open("hello.html","r")
content = f.read()

soup = BeautifulSoup(content,"html.parser")
titles = soup.findAll("h5")
for title in titles:
    print(title.text)

products = soup.findAll("div",class_='cards')

for product in products:
    title = product.h5.text
    price = product.a.text.split()[1]
    print(f"the product {title} costs {price}")


